// Example of running Angular under IWebBrowser2
// Copyright (C) Pavel Turbin, 2017.

#pragma once


class CBrowserEvent : public IDispatch
{
public:
    DWORD   m_dwCookie;
    HWND    m_hWnd;
    UINT    m_message;

    CBrowserEvent(HWND hWnd, UINT message) :
        m_hWnd(hWnd),
        m_message(message),
        m_dwCookie(0)
    {
    }

    virtual ~CBrowserEvent() {}
    STDMETHODIMP_(ULONG) AddRef()
    {
        return 1;
    }

    STDMETHODIMP_(ULONG) Release()
    {
        return 1;
    }

    STDMETHODIMP QueryInterface(REFIID iid, LPVOID* ppvObj)
    {
        if (!ppvObj)
            return E_POINTER;

        *ppvObj = NULL;
        if (IsEqualIID(iid, __uuidof(IUnknown)) ||
            IsEqualIID(iid, __uuidof(IDispatch)))
        {
            *ppvObj = this;
            return S_OK;
        }
        return E_NOINTERFACE;
    }

    STDMETHODIMP GetTypeInfoCount(UINT *pctinfo)
    {
        *pctinfo = 0;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags,
        DISPPARAMS *pdispparams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr)
    {
        if (dispIdMember == DISPID_DOCUMENTCOMPLETE)
        {
            PostMessage(m_hWnd, m_message, 0, 0);
        }

        return S_OK;
    }

    HRESULT  Attach(CComPtr<IWebBrowser2> &m_pBrowserApp)
    {
        CComPtr<IDispatch> spdispElement;
        if (m_pBrowserApp->QueryInterface(__uuidof(IDispatch), (void **)&spdispElement) != S_OK)
            return E_FAIL;

        return AtlAdvise(spdispElement, (LPDISPATCH)this, __uuidof(IDispatch), &m_dwCookie);
    }

    void Detouch(CComPtr<IWebBrowser2> &m_pBrowserApp)
    {
        AtlUnadvise(m_pBrowserApp, __uuidof(IDispatch), m_dwCookie);
    }
};



class CElementEvent : public IDispatch
{
public:
    CComPtr<IUnknown> m_spunkElem;
    DWORD   m_dwCookie;
    HWND    m_hWnd;
    UINT    m_message;

    CElementEvent(HWND hWnd, UINT message, IDispatch *pdisp) :
        m_hWnd(hWnd),
        m_message(message),
        m_dwCookie(0)
    {
        pdisp->QueryInterface(__uuidof(IUnknown), (void **)&m_spunkElem);
    }

    virtual ~CElementEvent() {}
    STDMETHODIMP_(ULONG) AddRef()
    {
        return 1;
    }

    STDMETHODIMP_(ULONG) Release()
    {
        return 1;
    }

    STDMETHODIMP QueryInterface(REFIID iid, LPVOID* ppvObj)
    {
        if (!ppvObj)
            return E_POINTER;

        *ppvObj = NULL;
        if (IsEqualIID(iid, __uuidof(IUnknown)) ||
            IsEqualIID(iid, __uuidof(IDispatch)))
        {
            *ppvObj = this;
            return S_OK;
        }
        return E_NOINTERFACE;
    }

    STDMETHODIMP GetTypeInfoCount(UINT *pctinfo)
    {
        *pctinfo = 0;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags,
        DISPPARAMS *pdispparams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr)
    {
        if (dispIdMember != DISPID_CLICK)
            return S_OK;

        PostMessage(m_hWnd, m_message, 0, 0);

        return S_OK;
    }

    HRESULT  Attach(CComPtr<IDispatch> &spdispElement)
    {
        return AtlAdvise(spdispElement, (LPDISPATCH)this, __uuidof(IDispatch), &m_dwCookie);
    }

    void Detouch()
    {
        AtlUnadvise(m_spunkElem, __uuidof(IDispatch), m_dwCookie);
    }
};


