// Example of running Angular under IWebBrowser2
// Copyright (C) Pavel Turbin, 2017.

#include "stdafx.h"
#include "MainDlg.h"


CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
END_OBJECT_MAP()


extern "C" int WINAPI wWinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPWSTR lpCmdLine,
    int nShowCmd)
{
    CoInitialize(NULL);
    SetProcessDPIAware();

    _Module.Init(ObjectMap, hInstance, NULL);
    CMainDlg dlg;
    dlg.DoModal();
    CoUninitialize();
    return 0;
}


LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    CenterWindow();

    // initialize Web Control form resources
    HRESULT hr = GetDlgControl(IDC_EXPLORER1, IID_IWebBrowser2, (void**)&m_pBrowserApp);
    if (hr != S_OK)
        return hr;

    // listen document load event
    m_BrowserEvent = new CBrowserEvent(m_hWnd, WM_BROWSER_DOCUMENTCOMPLETE);
    m_BrowserEvent->Attach(m_pBrowserApp);

    // navigate to resource ID
    LoadFromResource(IDR_HTML_MAIN);

    return hr;
}

LRESULT CMainDlg::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    UnregisterEventNotifications();

    m_BrowserEvent->Detouch(m_pBrowserApp);
    delete m_BrowserEvent;
    m_BrowserEvent = 0;

    if (m_pBrowserApp)
        m_pBrowserApp.Release();
    return 0;
}

LRESULT CMainDlg::OnBrowserDocumentComplete(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    ObtainHtmlDocument();
    UnregisterEventNotifications(); // in case of reload, drop old listeners
    RegisterEventNotifications();
    return S_OK;
}

LRESULT CMainDlg::OnFromBrowserToNative(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    OnFromBrowserToNative();
    return S_OK;
}

void CMainDlg::OnFromBrowserToNative()
{
    CComPtr<IHTMLElement>  fromNativeToBrowser;
    if (GetElement(L"fromNativeToBrowser", &fromNativeToBrowser) == S_OK)
    {
        fromNativeToBrowser->setAttribute(CComBSTR("value"), CComVariant(L"successfully"));
        fromNativeToBrowser->click();
    }
}

void    CMainDlg::RegisterEventNotifications()
{
    RegisterNotificationForControl(L"fromBrowserToNative", WM_FROM_BROWSER_TO_NATIVE);
}

void    CMainDlg::RegisterNotificationForControl(const WCHAR *elementId, UINT messageId)
{
    CComPtr<IDispatch> spdispElement;

    if (GetElementDispatch(elementId, &spdispElement) != S_OK)
        return;

    CElementEvent *pEvent = new CElementEvent(m_hWnd, messageId, spdispElement);
    if (pEvent->Attach(spdispElement) == S_OK)
        m_ElementsEvents.Add(pEvent);
}


void  CMainDlg::UnregisterEventNotifications()
{
    for (int i = 0; i<m_ElementsEvents.GetSize(); i++)
    {
        m_ElementsEvents[i]->Detouch();
        delete m_ElementsEvents[i];
    }
    m_ElementsEvents.RemoveAll();
}


LRESULT CMainDlg::OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    EndDialog(wID);
    return 0;
}

BOOL CMainDlg::LoadFromResource(UINT nRes)
{
    CAtlStringW strResourceURL;

    // it is possible to control IE version of Web control via regisry
    // https://blogs.msdn.microsoft.com/patricka/2015/01/12/controlling-webbrowser-control-compatibility/
    BOOL bRetVal = TRUE;
    LPWSTR lpszModule = new WCHAR[_MAX_PATH];

    if (GetModuleFileNameW(_Module.GetModuleInstance(), lpszModule, _MAX_PATH))
    {
        strResourceURL.Format(L"res://%s/%d", lpszModule, nRes);
        Navigate(strResourceURL);    
    }
    else
        bRetVal = FALSE;

    delete[] lpszModule;
    return bRetVal;
}

void CMainDlg::Navigate(LPCTSTR lpszURL, DWORD dwFlags /*= 0*/,
    LPCTSTR lpszTargetFrameName /*= NULL*/, LPCTSTR lpszHeaders /*= NULL*/,
    LPVOID lpvPostData /*= NULL*/, DWORD dwPostDataLen /*= 0*/)
{
    CComBSTR bstrURL = lpszURL;
    m_pBrowserApp->Navigate(bstrURL,
        NULL,
        NULL,
        NULL,
        NULL);
}

void CMainDlg::ObtainHtmlDocument()
{
    IDispatch *pdispDoc = NULL;
    m_pBrowserApp->get_Document(&pdispDoc);
    if (!pdispDoc)
        return;

    pdispDoc->QueryInterface(IID_IHTMLDocument2, (void **)&m_spHtmlDoc);
}


HRESULT CMainDlg::GetElementDispatch(const WCHAR *szElementId, IDispatch **ppdisp)
{
    CComPtr<IHTMLElementCollection> sphtmlAll;
    CComPtr<IDispatch> spdispElem;
    HRESULT hr = S_OK;

    *ppdisp = NULL;

    hr = m_spHtmlDoc->get_all(&sphtmlAll);
    if (sphtmlAll == NULL)
        return E_NOINTERFACE;

    hr = sphtmlAll->item(CComVariant(szElementId), CComVariant(), &spdispElem);
    if (spdispElem == NULL)
    {
        return E_NOINTERFACE;
    }

    *ppdisp = spdispElem;
    (*ppdisp)->AddRef();
    return hr;
}


HRESULT CMainDlg::GetElement(LPCTSTR szElementId, IHTMLElement **pphtmlElement)
{
    HRESULT hr = E_NOINTERFACE;
    CComPtr<IDispatch> spdispElem;

    hr = GetElementDispatch(szElementId, &spdispElem);

    if (spdispElem)
        hr = spdispElem->QueryInterface(__uuidof(IHTMLElement), (void **)pphtmlElement);
    return hr;
}


