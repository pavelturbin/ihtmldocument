// Example of running Angular under IWebBrowser2
// Copyright (C) Pavel Turbin, 2017.

#pragma once


#include <atlbase.h>
#include <atlwin.h>
#include <atlcom.h>
#include <strsafe.h>
#include <cstringt.h>
#include <atlhost.h>
#include <atlstr.h>
#include <OleCtl.h>
#include <ExDispid.h>
#include "ComEvents.h"
#include "resource.h"

#define WM_FROM_BROWSER_TO_NATIVE  (WM_USER+4919)
#define WM_BROWSER_DOCUMENTCOMPLETE (WM_USER+4919+1)


class CMainDlg : public CAxDialogImpl<CMainDlg>
{
public:
    CMainDlg() {}
    ~CMainDlg() {}

    enum { IDD = IDD_MAINDLG };

    BEGIN_MSG_MAP(CMainDlg)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
        COMMAND_ID_HANDLER(IDCANCEL, OnCancel)
        MESSAGE_HANDLER(WM_BROWSER_DOCUMENTCOMPLETE, OnBrowserDocumentComplete)
        MESSAGE_HANDLER(WM_FROM_BROWSER_TO_NATIVE, OnFromBrowserToNative)
    END_MSG_MAP()

    LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnBrowserDocumentComplete(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnFromBrowserToNative(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

private:
    void    OnFromBrowserToNative();
    BOOL    LoadFromResource(UINT nRes);
    void    Navigate(LPCTSTR lpszURL, DWORD dwFlags = 0,
        LPCTSTR lpszTargetFrameName = NULL, LPCTSTR lpszHeaders = NULL,
        LPVOID lpvPostData = NULL, DWORD dwPostDataLen = 0);

    HRESULT GetElementDispatch(const WCHAR *elementId, IDispatch **ppdisp);
    HRESULT GetElement(LPCTSTR szElementId, IHTMLElement **pphtmlElement);

    void    ObtainHtmlDocument();

    void    RegisterEventNotifications();
    void    UnregisterEventNotifications();

    void    RegisterNotificationForControl(const WCHAR *elementId, UINT messageId);


    CComPtr<IWebBrowser2> m_pBrowserApp;
    CComPtr<IHTMLDocument2> m_spHtmlDoc;
    CSimpleArray<CElementEvent *> m_ElementsEvents;

    CBrowserEvent          *m_BrowserEvent;
};


