// Example of running Angular under IWebBrowser2
// Copyright (C) Pavel Turbin, 2017.

#define VS_VERSION_INFO                 1
#define IDD_MAINDLG                     101
#define IDC_EXPLORER1                   1000
#define IDR_HTML_MAIN                   1001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
